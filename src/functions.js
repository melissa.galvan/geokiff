/* Revoir fonction */
export function axiosFunction(country, posX, scndPosX, posY, scndPosY) {
    posX -= 277;
    scndPosX -= 277;
    const axios = require('axios');

    axios.get("https://restcountries.eu/rest/v2/name/" + country)

        .then(function (response) {
            // Title or country name
            let title = document.createElement('h5');
            title.className = "title text-center";
            let ulInfos = document.createElement('ul');
            ulInfos.className = "card-body";
            let liCapital = document.createElement('li');
            let liLanguage = document.createElement('li');
            let liPopulation = document.createElement('li');

            let liMoney = document.createElement('li');

            let liLongLat = document.createElement('li');
            let liArea = document.createElement('li');
            let liRegion = document.createElement('li');
            let liFlag = document.createElement('li');
            liFlag.className = "text-center";
            let imgFlag = document.createElement('img');
            imgFlag.className = "imgFlag";

            // console.log(response.data);
            let playground = document.querySelector('#playground');
            let infos = document.querySelector('#infos');
            playground.addEventListener("mousemove", function (e) {
                if ((e.clientX >= posX + playground.offsetLeft && e.clientX <= scndPosX + playground.offsetLeft) && (e.clientY >= posY && e.clientY <= scndPosY)) {
                    for (const iterator of response.data) {
                        infos.innerHTML = "";

                        playground.appendChild(infos);
                        infos.appendChild(ulInfos);
                        ulInfos.append(title, liCapital, liLanguage, liPopulation, 
                                liMoney, liLongLat, liArea, liRegion, liFlag);

                        liFlag.appendChild(imgFlag);
                        imgFlag.setAttribute("src", iterator.flag);

                        title.innerHTML = iterator.translations.fr;
                        liCapital.innerHTML = "Capitale : " + iterator.capital;
                        for (const item of iterator.languages) {
                            liLanguage.innerHTML = "Langue : " + item.nativeName + " / " + item.name;
                        }
                        liPopulation.innerHTML = "Population : " + iterator.population;
                        liLongLat.innerHTML = "Lattitude,Longitude : " + iterator.latlng;
                        liArea.innerHTML = "Superficie : " + iterator.area + " km²";
                        liRegion.innerHTML = "Continent : " + iterator.subregion;
                        for (const item of iterator.currencies) {
                            liMoney.innerHTML = "Monnaie : " + item.name + " " + item.symbol;
                        }
                    }
                }
                console.log(e);

            })
        })


        .catch(function (error) {
            console.log(error);
        })
}